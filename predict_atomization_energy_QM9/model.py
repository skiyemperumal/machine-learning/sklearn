#Description: Demonstration of KernelRidge Regression 
#using Scikit-learn for predicting Atomization Energies 
#---------------------------------------------------
# Required Files: data_AE.csv 
# Required Packages: sklearn, numpy, matplotlib, pandas
#                    and memory_profiler if you want memory
#                    information of your run 
# Usage: python model.py 
#
# Output: 
#         Score of predition is printed out 
#---------------------------------------------------
# Author: Satish Kumar Iyemperumal e-mail: satish2414 [at] gmail.com
# Date:   Feb 13, 2018 
#---------------------------------------------------

from sklearn.preprocessing import StandardScaler
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from memory_profiler import profile 
#See https://pypi.python.org/pypi/memory_profiler for more info
#import resource, signal

'''
#Memory Management
def time_exceeded(signo, frame):
    print("Time's up!")
    raise SystemExit(1)

def memory_limit(updated_soft, seconds):
    soft, hard = resource.getrlimit(resource.RLIMIT_DATA)
    resource.setrlimit(resource.RLIMIT_DATA, (updated_soft, hard)) 
    resource.setrlimit(resource.RLIMIT_RSS, (updated_soft, hard)) 
    resource.setrlimit(resource.RLIMIT_CPU, (seconds, hard))
    signal.signal(signal.SIGXCPU, time_exceeded)
''' 
#Read data
@profile
def my_func(): 
    df = pd.read_csv('data_AE.csv') 
    colnames = df.columns.values
    Xcols = colnames[1:len(colnames)] 
    x0, x1, x2, x3 = [np.array( df.iloc[:, n].tolist() ) for n in range(len(Xcols))]
    y = np.array( df.iloc[:,0].tolist() )
    #sklearn compatible matrix of dimensions
    X_raw = np.c_[x0, x1, x2, x3] 

#Scaling for SVM
    scaler = StandardScaler()
    scaler.fit(X_raw)
    X = scaler.transform(X_raw)

#My iMac(i5/8Gig RAM) couldn't handle more than 5000. So use train_size wisely
    train_size = [1000] 
    list_score = []
    random_state = np.random.randint(100,size=1) 
    for i in train_size:
        list_score = []
        for j in random_state:
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, train_size=i, random_state=j)
            
            kr = KernelRidge(kernel='rbf')
            kr.fit(X_train, y_train)
            y_pred = kr.predict(X_test)
            y_true = y_test
            score = mean_absolute_error(y_true, y_pred)
            list_score.append(score)
        
        avg_score = sum(list_score)/len(list_score)
        print "Average Score: ", avg_score

if __name__ == '__main__':
    my_func()
