#Description: Demonstration of Nested cross validation 
#using Scikit-learn 
#---------------------------------------------------
# Required Files: data_AE.csv 
# Required Packages: sklearn, numpy, matplotlib, pandas 
# Usage: python model.py 
#
# Output: 
#         plot.png: Nested/Non-nested score comparison 
#---------------------------------------------------
# Author: Satish Kumar Iyemperumal e-mail: satish2414 [at] gmail.com
# Date:   Feb 13, 2018 
#---------------------------------------------------

from sklearn.preprocessing import StandardScaler
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import train_test_split, cross_val_score, KFold, GridSearchCV 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Atomization energies obtained from QM9, Other manually built descriptors 
#as per autocorrelation functions (see 10.1021/acs.jpca.7b08750)
#Read data
df = pd.read_csv('data_AE.csv') 
colnames = df.columns.values
Xcols = colnames[1:len(colnames)] 
x0, x1, x2, x3 = [np.array( df.iloc[:, n].tolist() ) for n in range(len(Xcols))]
y = np.array( df.iloc[:,0].tolist() )
#sklearn compatible matrix of dimensions
X_raw = np.c_[x0, x1, x2, x3] 

#Scaling for SVM
scaler = StandardScaler()
scaler.fit(X_raw)
X = scaler.transform(X_raw)

#Split training and testing data
#Use fractional test/train_size for using all data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=50, 
                train_size=100, random_state=40)

param_grid={"alpha": [1e0, 0.1, 1e-2, 1e-3],
               "gamma": np.logspace(-2, 2, 5)}

num_trials = 5
plotdatay1, plotdatay2 = [], []
for i in range(num_trials):
    print i
    regr = KernelRidge(kernel='rbf')
    kfold = KFold(n_splits=5, shuffle=True, random_state=i)
    grid_search = GridSearchCV(regr, param_grid, cv=kfold)

    grid_search.fit(X_train, y_train)
    non_nested_score = grid_search.score(X_test, y_test)
    print "Non-Nested Test set score: %.2f" %non_nested_score
    nested_score = cross_val_score(grid_search, X=X_test, y=y_test, cv=kfold)
    score = nested_score.mean()
    plotdatay1.append(non_nested_score)
    plotdatay2.append(score)

    print "Nested Test set score: %.2f" %score
    print "Best estimator:\n{}".format(grid_search.best_estimator_)
    print "Best parameters: {}".format(grid_search.best_params_)

print "Average Nested Score:", sum(plotdatay2)/len(plotdatay2)
#Plotting
plt.plot(plotdatay1, c='k',label="non_nested_score")
plt.plot(plotdatay2, c='red', label="nested_score")
plt.ylabel('Score')
plt.xlabel('Run #')
plt.legend()
plt.savefig('plot.png')
